import cv2
import numpy as np

def save_images(blackAndWhiteImage3, dst, real_image, up, down, left, right, c):
    cv2.imwrite(f'output1/mask/seed_mask{c}.png', blackAndWhiteImage3[up:down, left:right])
    cv2.imwrite(f'output1/trans/seed_transp{c}.png', dst)
    cv2.imwrite(f'output1/img/seed{c}.png', real_image[up:down, left:right])

def create_images(real_image, up, down, left, right, blackAndWhiteImage, c):
    # Для transparent img
    b, g, r = cv2.split(real_image[up:down, left:right])
    rgba = [b, g, r, blackAndWhiteImage[up:down, left:right]]
    dst = cv2.merge(rgba, 4)

    blackAndWhiteImage3 = cv2.cvtColor(blackAndWhiteImage, cv2.COLOR_GRAY2BGR)
    blackAndWhiteImage3[blackAndWhiteImage == 255] = (100, 100, 100)

    save_images(blackAndWhiteImage3, dst, real_image, up, down, left, right, c)

def find_seed_bounds_by_contours(contours, hierarchy, image, real_image, blackAndWhiteImage):
    height, width, depth = image.shape
    for c in range(len(contours)):
        blank = np.zeros((image.shape), np.uint8)
        cv2.drawContours(blank, [contours[c]], -1, (255, 255, 255), -1)

        left = width
        right = 0
        up = height
        down = 0
        flag = 0
        for i in range(height):
            if (255, 255, 255) in blank[i]:
                for j in range(width):
                    if (blank[i][j][0] == 255):
                        if (up == height and blank[i][j][0] == 255):
                            up = i
                        if (i > down and blank[i][j][0] == 255):
                            down = i
                        if (j < left):
                            left = j
                        if (j > right):
                            right = j
                        if (255, 255, 255) not in blank[i + 1]:
                            flag = 1
                            break
                if (flag == 1):
                    break
        create_images(real_image, up, down, left, right, blackAndWhiteImage, c)
        print(f"записано {c + 1}/{len(contours)} картинок")

#размеченное изображение (пока что корректно только для пшеницы)
image = cv2.imread('label.png')
#исходное изображение
real_image = cv2.imread('img.png')
grayImage = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
(thresh, blackAndWhiteImage) = cv2.threshold(grayImage, 73, 255, cv2.THRESH_BINARY)

contours, hierarchy = cv2.findContours(blackAndWhiteImage, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

find_seed_bounds_by_contours(contours, hierarchy, image, real_image, blackAndWhiteImage)